import argparse
import os
import math
import numpy as np
from PIL import Image
import multiprocessing as mp


def file_to_image(filename, chunksize, outdir):
    print(f'Encoding bytes in file \'{filename}\' as an image...')
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)
        
    pool = mp.Pool(mp.cpu_count())
    jobs = []

    pixels = []
    filenum = 0

    with open(filename, "rb") as f:
        file_size = os.fstat(f.fileno()).st_size
        num_chunks = math.ceil(file_size / chunksize)
        print(f"Processing file with {file_size} bytes. "
              f"Will be processed in {num_chunks} chunks")
        byte = f.read(chunksize)
        while byte != b"":
            pixels = np.frombuffer(byte, dtype=np.uint8)
            filenum += 1
            jobs.append(pool.apply_async(array_to_image(pixels, filenum, outdir)))
            byte = f.read(chunksize)

    # wait for all jobs to complete
    for job in jobs:
        try:
            job.get()
        except TypeError:
            pass
    pool.close()
    print("DONE!")


def array_to_image(pixels, filenum, outdir):
    print(f"Converting file chunk {filenum} to image")
    pixels_len = len(pixels)
    meta_size = math.ceil(math.ceil(math.sqrt(pixels_len)) / 255)
    x = math.ceil(math.sqrt(pixels_len + meta_size))
    y = math.ceil(pixels_len / x)
    padding_size = (x * y) - ((pixels_len - 1) + meta_size)
    nparray = np.concatenate((create_meta(meta_size, padding_size), pixels))
    nparray.resize(x * y)
    nparray = nparray.reshape((x, y))
    nparray = nparray.astype('uint8')
    im = Image.fromarray(nparray)
    im.save(f"./{outdir}/carretera-{filenum}.png")


def create_meta(meta_size, padding_size):
    meta = []
    for i in range(meta_size):
        if padding_size > 255:
            meta.append(255)
            padding_size -= 255
        elif padding_size > 0:
            meta.append(padding_size)
            padding_size = 0
        else:
            meta.append(0)
    return np.array(meta)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Encode file.')
    parser.add_argument('-i', '--inputfile', help='File to encode', required=True)
    parser.add_argument('-o', '--outdir', help='Output directory name', required=False, default="output")
    parser.add_argument('-c', '--chunksize', type=int, help='Chunksize to use', required=True)
    args = parser.parse_args()
    file_to_image(args.inputfile, args.chunksize, args.outdir)
