import math
import os
import sys
import time
from tkinter import filedialog
from tkinter import ttk
from tkinter import Label
from tkinter import Button
from tkinter import Tk
from tkinter import Entry
from threading import Thread
from subprocess import Popen
from subprocess import check_call

encode = None
decode = None

mode = Tk()
mode.title("Infinite Cloud Storage")

window_x = 640
window_y = 275
window_geometry = f"{window_x}x{window_y}"
mode.geometry(window_geometry)

std_margin = 20
min_margin = 5

def open_destination_dir(path):
    full_path = os.path.dirname(os.path.abspath(__file__))
    print(f"opening directory {path} under {full_path}")
    if sys.platform == 'darwin':
        full_path += f"/{path}"
        check_call(['open', '--', full_path])
    elif sys.platform == 'linux2' :
        full_path += f"/{path}"
        check_call(['xdg-open', '--', full_path])
    elif sys.platform == 'linux':
        full_path += f"/{path}"
        check_call(['xdg-open', full_path])
    elif sys.platform == 'win32':
        full_path += f"\\{path}"
        check_call(['explorer', full_path])

#################### E N C O D E ###################

def open_encode_out(path):
    open_destination_dir(path)

def quit_encode():
    encode.destroy()
    sys.exit(0)


def upload_file():
    encode.filename = filedialog.askopenfilename(
        initialdir="./",
        title="Select the file to encode",
        filetypes=(("all files", "*.*"), ("all files", "*.*")))

    if len(encode.filename) > 0:
        outdir_label = Label(encode, text="Output directory name:")
        outdir_label.place(x=std_margin, y=3 * std_margin-min_margin)
        encode.update()
        outdir_input = Entry(encode)
        outdir_input.insert(0,"output")
        outdir_input.place(x=std_margin + outdir_label.winfo_width() + min_margin, y=3 * std_margin-min_margin)
        
        filename_label = Label(encode, text=encode.filename)
        filename_label.place(x=std_margin, y=4 * std_margin)
        encode.update()
        upload_button = Button(encode, text="Start", command= lambda: start_upload(outdir_input.get()))
        upload_button.place(
            x=std_margin + filename_label.winfo_width(),
            y=(4 * std_margin) - min_margin)
        print(encode.filename)


def start_upload(outdir):
    print("Starting encode...")
    progress = ttk.Progressbar(
        orient="horizontal", length=600, mode="determinate")
    progress.place(x=std_margin, y=5 * std_margin + min_margin)
    progress.configure(maximum=100)
    chunksize = 10000000
    filesize = os.path.getsize(encode.filename)
    num_chunks = math.ceil(filesize / chunksize)

    command = [
        sys.executable, 'encode.py', 
        '-i', str(encode.filename),
        '-o', str(outdir),
        '-c', str(chunksize)
    ]
    
    Popen(command, shell=False)
    Thread(target=update_progress_bar(progress, num_chunks, outdir)).start


def update_progress_bar(progress, num_chunks, outdir):
    upload_complete_label = Label(encode, text="")
    upload_complete_label.place(x=std_margin, y=6 * std_margin + min_margin)

    step_size = 100 / num_chunks
    for i in range(1, num_chunks + 1):
        while not os.path.exists(f'./{outdir}/carretera-{i}.png'):
            time.sleep(0.2)

        next_step = i * step_size
        if next_step >= 100:
            progress.step(step_size - (100 - next_step) - 0.001)
        else:
            progress.step(step_size)

        upload_complete_label.config(
            text=f'{int(next_step)}% Encoding Chunk {i}/{num_chunks}')
        encode.update()

    upload_complete_label.config(text=f'{int(next_step)}% Encoding complete!')
    exit_button = Button(encode, text="Exit", command=quit_encode)
    exit_button.place(x=std_margin, y=8 * std_margin + min_margin)
    browse_to_dir_button = Button(encode, text="Open output directory", command= lambda: open_encode_out(outdir))
    browse_to_dir_button.place(x=std_margin*4 + min_margin, y=8 * std_margin + min_margin)


def init_encode():
    global encode
    encode = Tk()
    encode.title("Infinite Cloud Storage")
    encode.geometry(window_geometry)
    upload_label = Label(encode, text="Select the file to encode:")
    upload_label.place(x=std_margin, y=std_margin)
    encode.update()
    select_file_button = Button(encode, text="Open", command=upload_file)
    select_file_button.place(
        x=std_margin + upload_label.winfo_width(), y=std_margin - min_margin)


#################### D E C O D E ###################

def open_decode_out():
    open_destination_dir('')

def quit_decode():
    decode.destroy()
    sys.exit(0)


def start_decode():
    print("Starting decode...")
    progress = ttk.Progressbar(
        orient="horizontal", length=600, mode="determinate")
    progress.place(x=std_margin, y=4 * std_margin + min_margin)
    progress.configure(maximum=100)

    num_files = len([name for name in os.listdir(decode.filename)])
    
    command = [
        sys.executable, 'decode.py', 
        '-i', str(decode.filename), 
        '-o', 'change_my_name'
    ]
    
    Popen(command, shell=False)
    Thread(target=update_decode_progress_bar(progress, num_files)).start


def update_decode_progress_bar(progress, num_files):
    upload_complete_label = Label(decode, text="")
    upload_complete_label.place(x=std_margin, y=5 * std_margin + min_margin)

    step_size = 100 / num_files
    for i in range(1, num_files + 1):
        time.sleep(0.1)

        next_step = i * step_size
        if next_step >= 100:
            progress.step(step_size - (100 - next_step) - 0.001)
        else:
            progress.step(step_size)

        upload_complete_label.config(
            text=f'{int(next_step)}% Decoding Chunk {i}/{num_files}')
        decode.update()

    upload_complete_label.config(text=f'{int(next_step)}% Decoding complete!')
    exit_button = Button(decode, text="Exit", command=quit_decode)
    exit_button.place(x=std_margin, y=8 * std_margin + min_margin)
    browse_to_dir_button = Button(decode, text="Open output directory", command=open_decode_out)
    browse_to_dir_button.place(x=std_margin*4 + min_margin, y=8 * std_margin + min_margin)


def decode_file():
    decode.filename = filedialog.askdirectory(
        initialdir="./", title="Select folder to decode")
    if len(decode.filename) > 0:
        filename_label = Label(decode, text=decode.filename)
        filename_label.place(x=std_margin, y=3 * std_margin)
        decode.update()
        upload_button = Button(decode, text="decode", command=start_decode)
        upload_button.place(
            x=std_margin + filename_label.winfo_width(),
            y=(3 * std_margin) - min_margin)
        print(decode.filename)


def init_decode():
    global decode
    decode = Tk()
    decode.title("Infinite Cloud Storage")
    decode.geometry(window_geometry)
    upload_label = Label(decode, text="Select a folder to decode:")
    upload_label.place(x=std_margin, y=std_margin)
    decode.update()
    select_file_button = Button(decode, text="Open", command=decode_file)
    select_file_button.place(
        x=std_margin + upload_label.winfo_width(), y=std_margin - min_margin)


#################### M O D E ###################


def select_encode():
    mode.destroy()
    init_encode()
    encode.mainloop()


def select_decode():
    mode.destroy()
    init_decode()
    decode.mainloop()


#Labels
encode_select_label = Label(mode, text="File To Images")
encode_select_label.place(x=127, y=3 * std_margin)
mode.update()
decode_select_label = Label(mode, text="Images To File")
decode_select_label.place(x=391, y=3 * std_margin)
#Buttons
encode_select_button = Button(
    encode, text="Encode", command=select_encode, height=10, width=25)
encode_select_button.place(x=77, y=80)
decode_select_button = Button(
    decode, text="Decode", command=select_decode, height=10, width=25)
decode_select_button.place(x=343, y=80)

#################### L O O P ###################
mode.mainloop()
