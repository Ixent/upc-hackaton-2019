import argparse
import numpy as np
import math
import os
from PIL import Image


def image_to_file(imagefile, output_file):
    print('Decoding image {} to file'.format(imagefile))
    im = Image.open(imagefile)
    pixels = np.asarray(im)
    pixels = purify(pixels)

    with open(output_file, "ab") as f:
        f.write(pixels.tobytes())


def purify(np_data):
    (x, y) = np_data.shape
    np_flat = np_data.flatten()
    np_len = len(np_flat)
    meta_size = math.ceil(math.ceil(math.sqrt(np_len)) / 255)
    padding_size = np.sum(np_flat[:meta_size])
    print(f"meta size is: {meta_size} | padding size is {padding_size}")
    padding_start = int(np_len - padding_size)
    return np_flat[meta_size:padding_start + 1]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Decode file.')
    parser.add_argument(
        '-o', '--output_file', help='File to decode', required=True)
    parser.add_argument(
        '-i', '--input_folder', help='Folder containing images', required=True)

    args = parser.parse_args()
    if os.path.exists(args.output_file):
        os.remove(args.output_file)

    for root, dirs, images in os.walk(args.input_folder):
        print(f"found {len(images)} images to decode")
        images.sort(key=lambda x: int(x[x.rfind('-') + 1:x.rfind('.')]))
        for image in images:
            image_to_file(root + '/' + image, args.output_file)
